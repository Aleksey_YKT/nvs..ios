//
//  PolyclinicInfoViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class PolyclinicInfoViewController: UIViewController {

    @IBOutlet weak var siteLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var registryPhoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var login = ""
    var password = ""
    var polyclinic = Polyclinic()
    
    override func viewDidLoad() {

        super.viewDidLoad()

        let userData = loadUserData()
        parseJSON(userData.data(using: .utf8)!)
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateView() {
        nameLabel.text = polyclinic.name
        siteLabel.text = polyclinic.site
        addressLabel.text = polyclinic.address_street + " " + polyclinic.address_number
        registryPhoneLabel.text = polyclinic.registry_phone
        emailLabel.text = polyclinic.email
        
        if polyclinic.site == "" {
            siteLabel.text = "Нет"
        }
        
        if polyclinic.registry_phone == "" {
            registryPhoneLabel.text = "Нет"
        }
        
        if polyclinic.email == "" {
            emailLabel.text = "Нет"
        }
    }

    func getDataFromServer() {
        let getPolyclinicInfo = "?login=" + login + "&password=" + password
        
        let request = URLRequest(url: URL(string: siteURL + getPolyclinicInfoURL + getPolyclinicInfo)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseRecordsData(data)
            }
            
        })
        task.resume()
    }
    
    func parseRecordsData(_ data: Data) {
        do {
            guard let polyclinicInfoData = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            print(polyclinicInfoData)
            
            polyclinic.id = polyclinicInfoData["id"] as! Int
            polyclinic.name = polyclinicInfoData["name"] as! String
            polyclinic.site = polyclinicInfoData["site"] as! String
            polyclinic.address_street = polyclinicInfoData["address_street"] as! String
            polyclinic.address_number = polyclinicInfoData["address_number"] as! String
            polyclinic.registry_phone = polyclinicInfoData["registry_phone"] as! String
            polyclinic.reception_phone = polyclinicInfoData["reception_phone"] as! String
            polyclinic.email = polyclinicInfoData["email"] as! String
            
            DispatchQueue.main.async {
                self.updateView()
            }
        } catch {
            print(error)
        }
    }
    
    
    func loadUserData() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let user_login = json["login"] as? String {
                login = user_login
            }
            
            if let user_password = json["password"] as? String {
                password = user_password
            }
        } catch {
            print(error)
        }
    }

}
