//
//  Patient.swift
//  nvs
//
//  Created by Aleksey Ivanov on 04.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

class Patient {
    var id: Int = 0
    var surname: String = ""
    var name: String = ""
    var patronymic: String = ""
    var passport_number: String = ""
    var date_of_birth: String = ""
    var residential_address: String = ""
    var phone_number: String = ""
    var policy_number: String = ""
    var snils_number: String = ""
}
