//
//  Polyclinic.swift
//  nvs
//
//  Created by Aleksey Ivanov on 04.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

class Polyclinic {
    var id: Int = 0
    var name: String = ""
    var site: String = ""
    var address_street: String = ""
    var address_number: String = ""
    var registry_phone: String = ""
    var reception_phone: String = ""
    var email: String = ""
}
