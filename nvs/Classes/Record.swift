//
//  Record.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

class Record {
    var id: Int = 0
    var date: String = ""
    var doctor: String = ""
    var specilality: String = ""
    var cabinet: String = ""
    var phone: String = ""
}
