//
//  Speciality.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

class Speciality {
    var id: Int = 0
    var name: String = ""
}
