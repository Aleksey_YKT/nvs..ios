//
//  Doctor.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

class Doctor {
    var id: Int = 0
    var surname: String = ""
    var name: String = ""
    var patronymic: String = ""
    var speciality: String = ""
    var cabinet_number: String = ""
    var cabinet_phone: String = ""
}
