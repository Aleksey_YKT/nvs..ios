//
//  MyInfoViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class MyInfoViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var passportLabel: UILabel!
    @IBOutlet weak var snilsLabel: UILabel!
    @IBOutlet weak var policyLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var login = ""
    var password = ""
    var patient = Patient()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let userData = loadUserData()
        parseJSON(userData.data(using: .utf8)!)
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func updateView() {
        nameLabel.text = patient.surname + " " + patient.name + " " + patient.patronymic
        passportLabel.text = patient.passport_number
        addressLabel.text = patient.residential_address
        phoneLabel.text = patient.phone_number
        policyLabel.text = patient.policy_number
        snilsLabel.text = patient.snils_number
        
        if patient.phone_number == "" {
            phoneLabel.text = "Нет"
        }
    }
    
    func getDataFromServer() {
        let getPatientInfo = "?login=" + login + "&password=" + password
        
        let request = URLRequest(url: URL(string: siteURL + getPatientInfoURL + getPatientInfo)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseRecordsData(data)
            }
            
        })
        task.resume()
    }
    
    func parseRecordsData(_ data: Data) {
        do {
            guard let patientInfoData = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            patient.id = patientInfoData["id"] as! Int
            patient.surname = patientInfoData["surname"] as! String
            patient.name = patientInfoData["name"] as! String
            patient.patronymic = patientInfoData["patronymic"] as! String
            patient.passport_number = patientInfoData["passport_number"] as! String
            patient.date_of_birth = patientInfoData["date_of_birth"] as! String
            patient.residential_address = patientInfoData["residential_address"] as! String
            patient.phone_number = patientInfoData["phone_number"] as! String
            patient.policy_number = patientInfoData["policy_number"] as! String
            patient.snils_number = patientInfoData["snils_number"] as! String
            
            DispatchQueue.main.async {
                self.updateView()
            }
        } catch {
            print(error)
        }
    }
    
    
    func loadUserData() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let user_login = json["login"] as? String {
                login = user_login
            }
            
            if let user_password = json["password"] as? String {
                password = user_password
            }
        } catch {
            print(error)
        }
    }


}
