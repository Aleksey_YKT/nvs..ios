//
//  RecordSelectTimeTableViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordSelectTimeTableViewController: UITableViewController {

    var speciality = Speciality()
    var doctor = Doctor()
    var login = ""
    var password = ""
    
    var times = [Time]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return times.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timeTableCell", for: indexPath) as! RecordSelectTimeTableViewCell
        
        cell.timeDateLabel?.text = CustomDateFormatter(recordTime: times[(indexPath as NSIndexPath).row].date)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "openRecordConfirmView" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! RecordConfirmViewController
                destinationController.login = login
                destinationController.password = password
                destinationController.speciality = speciality
                destinationController.doctor = doctor
                destinationController.time = times[(indexPath as NSIndexPath).row]
            }
        }
    }
    
    func getDataFromServer() {
        let getTimes = "?login=" + login + "&password=" + password + "&doctor=" + String(doctor.id)
        
        let request = URLRequest(url: URL(string: siteURL + getRecordTimesURL + getTimes)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseTimesData(data)
                self.tableView.reloadData()
            }
            
        })
        task.resume()
    }
    
    func parseTimesData(_ data: Data) {
        do {
            guard let timesData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            for timeData in timesData {
                let time = Time()
                
                time.id = timeData["id"] as! Int
                time.date = timeData["date"] as! String
                
                times.append(time)
            }
            
        } catch {
            print(error)
        }
    }
}
