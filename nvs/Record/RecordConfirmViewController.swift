//
//  RecordConfirmViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordConfirmViewController: UIViewController {

    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var speciality = Speciality()
    var doctor = Doctor()
    var time = Time()
    var login = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        specialityLabel.text = speciality.name
        doctorLabel.text = doctor.surname + " " + doctor.name + " " + doctor.patronymic
        timeLabel.text = CustomDateFormatter(recordTime: time.date)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func confirmButtonPressed(_ sender: Any) {
        let requestParams = "login=" + login + "&password=" + password + "&time=" + String(time.id) + "&doctor=" + String(doctor.id)
        
        var request = URLRequest(url: URL(string: siteURL + postRecordPatientURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере")
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Проверьте правильность введенных данных", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                _ = String(data: data, encoding: .utf8)
                
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "backToMenuView", sender: self)
                }
                
                let alertMessage = UIAlertController(title: "Готово", message: "Вы успешно записались к врачу", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
        
        task.resume()
    }
}
