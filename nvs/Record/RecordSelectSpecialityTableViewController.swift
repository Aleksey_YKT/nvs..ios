//
//  RecordSelectSpecialityTableViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordSelectSpecialityTableViewController: UITableViewController {
    
    var login = ""
    var password = ""

    var specialities = [Speciality]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let userData = loadUserData()
        parseJSON(userData.data(using: .utf8)!)
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return specialities.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "specialityTableCell", for: indexPath) as! RecordSelectSpecialityTableViewCell
        
        cell.nameLabel?.text = specialities[(indexPath as NSIndexPath).row].name
    
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "openRecordSelectDoctorView" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! RecordSelectDoctorTableViewController
                destinationController.speciality = specialities[(indexPath as NSIndexPath).row]
                destinationController.login = login
                destinationController.password = password
            }
        }
    }
    
    func getDataFromServer() {
        let getSpecialities = "?login=" + login + "&password=" + password
        
        let request = URLRequest(url: URL(string: siteURL + getRecordSpecialitiesURL + getSpecialities)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseSpecialitiesData(data)
                self.tableView.reloadData()
            }
            
        })
        task.resume()
    }
    
    func parseSpecialitiesData(_ data: Data) {
        do {
            guard let specialitiesData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            for specialityData in specialitiesData {
                let spec = Speciality()
                
                spec.id = specialityData["id"] as! Int
                spec.name = specialityData["name"] as! String
                
                specialities.append(spec)
            }
            
        } catch {
            print(error)
        }
    }

    func loadUserData() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let user_login = json["login"] as? String {
                login = user_login
            }
            
            if let user_password = json["password"] as? String {
                password = user_password
            }
        } catch {
            print(error)
        }
    }
}
