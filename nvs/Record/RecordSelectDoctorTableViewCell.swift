//
//  RecordSelectDoctorTableViewCell.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordSelectDoctorTableViewCell: UITableViewCell {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorPhoneNumberLabel: UILabel!
    @IBOutlet weak var doctorCabinetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
