//
//  RecordSelectDoctorTableViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordSelectDoctorTableViewController: UITableViewController {

    var speciality = Speciality()
    var login = ""
    var password = ""
    
    var doctors = [Doctor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return doctors.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "doctorTableCell", for: indexPath) as! RecordSelectDoctorTableViewCell
        
        cell.doctorNameLabel?.text = doctors[(indexPath as NSIndexPath).row].surname + " " + doctors[(indexPath as NSIndexPath).row].name + " " + doctors[(indexPath as NSIndexPath).row].patronymic
        
        cell.doctorCabinetLabel?.text = doctors[(indexPath as NSIndexPath).row].cabinet_number
        cell.doctorPhoneNumberLabel?.text = doctors[(indexPath as NSIndexPath).row].cabinet_phone
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "openRecordSelectTimeView" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! RecordSelectTimeTableViewController
                destinationController.login = login
                destinationController.password = password
                destinationController.speciality = speciality
                destinationController.doctor = doctors[(indexPath as NSIndexPath).row]
            }
        }
    }

    func getDataFromServer() {
        let getDoctods = "?login=" + login + "&password=" + password + "&speciality=" + String(speciality.id)
        
        let request = URLRequest(url: URL(string: siteURL + getRecordDoctrosURL + getDoctods)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseDoctorsData(data)
                self.tableView.reloadData()
            }
            
        })
        task.resume()
    }
    
    func parseDoctorsData(_ data: Data) {
        do {
            guard let doctorsData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            for doctorData in doctorsData {
                let doctor = Doctor()
                
                doctor.id = doctorData["id"] as! Int
                doctor.surname = doctorData["surname"] as! String
                doctor.name = doctorData["name"] as! String
                doctor.patronymic = doctorData["patronymic"] as! String
                doctor.cabinet_number = doctorData["cabinet"] as! String
                doctor.cabinet_phone = doctorData["cabinet_phone"] as! String
                
                doctors.append(doctor)
            }
            
        } catch {
            print(error)
        }
    }
}
