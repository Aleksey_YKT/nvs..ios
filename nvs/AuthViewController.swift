//
//  ViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 02.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginTextField: UITextField!
    
    var login = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        let userData = loadUserData()
        
        loginTextField.isHidden = true
        passwordTextField.isHidden = true
        loginButton.isHidden = true
        
        if (userData != "") {
            DispatchQueue.main.async {
                self.parseJSON(userData.data(using: .utf8)!)
                self.loginUser(userLogin: self.login, userPassword: self.password)
            }
        } else {
            loginTextField.isHidden = false
            passwordTextField.isHidden = false
            loginButton.isHidden = false
        }
        
        self.navigationItem.setHidesBackButton(true, animated:true)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        let userLogin = loginTextField.text!
        let userPassword = passwordTextField.text!
        
        if (userLogin != "" && userPassword != "") {
            loginUser(userLogin: userLogin, userPassword: userPassword)
        } else {
            let alertMessage = UIAlertController(title: "Ошибка", message: "Заполните поля", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
    }

    func loginUser(userLogin: String, userPassword: String) {
        let requestParams = "login=" + userLogin + "&password=" + userPassword
        
        var request = URLRequest(url: URL(string: siteURL + loginUserURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере")
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Проверьте правильность введенных данных", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                let responseString = String(data: data, encoding: .utf8)
                DispatchQueue.main.async {
                    DispatchQueue.main.async {
                        print("Ваш запрос отправлен")
                        print(responseString as String!)
                        
                        self.saveUserData(userLogin: userLogin, userPassword: userPassword)
                        self.performSegue(withIdentifier: "openMenuView", sender: self)
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func loadUserData() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    func saveUserData(userLogin: String, userPassword: String) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            var userData = [String: String]()
            userData["login"] = userLogin
            userData["password"] = userPassword
            
            var responseString = userData.description
            responseString.remove(at: responseString.startIndex)
            responseString.remove(at: responseString.index(before: responseString.endIndex))
            responseString = "{" + responseString + "}"
            print(responseString)
            
            // Сохранение в файл
            do {
                try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                print("Файл сохранен")
            } catch {
                print("Ошибка при сохранении в файл")
            }
        }
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let user_login = json["login"] as? String {
                login = user_login
            }
            
            if let user_password = json["password"] as? String {
                password = user_password
            }
        } catch {
            print(error)
        }
    }
}

