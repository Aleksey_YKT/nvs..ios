//
//  DoctorsInfoTableViewCell.swift
//  nvs
//
//  Created by Aleksey Ivanov on 04.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class DoctorsInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var cabinetLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
