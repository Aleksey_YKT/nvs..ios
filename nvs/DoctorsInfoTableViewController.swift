//
//  DoctorsInfoTableViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 04.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class DoctorsInfoTableViewController: UITableViewController {

    var login = ""
    var password = ""
    var doctors = [Doctor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let userData = loadUserData()
        parseJSON(userData.data(using: .utf8)!)
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return doctors.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "doctorsTableCell", for: indexPath) as! DoctorsInfoTableViewCell
        
        cell.nameLabel?.text = doctors[(indexPath as NSIndexPath).row].surname + " " + doctors[(indexPath as NSIndexPath).row].name + " " + doctors[(indexPath as NSIndexPath).row].patronymic
        
        cell.specialityLabel?.text = doctors[(indexPath as NSIndexPath).row].speciality
        cell.phoneLabel?.text = doctors[(indexPath as NSIndexPath).row].cabinet_phone
        cell.cabinetLabel?.text = doctors[(indexPath as NSIndexPath).row].cabinet_number
        
        return cell
    }
    
    func getDataFromServer() {
        let getDoctorsInfo = "?login=" + login + "&password=" + password
        
        let request = URLRequest(url: URL(string: siteURL + getDoctorsInfoURL + getDoctorsInfo)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseDoctorsInfoData(data)
                self.tableView.reloadData()
            }
            
        })
        task.resume()
    }
    
    func parseDoctorsInfoData(_ data: Data) {
        do {
            guard let DoctorsInfoData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            for doctorInfoData in DoctorsInfoData {
                let doctor = Doctor()
                
                doctor.id = doctorInfoData["id"] as! Int
                doctor.surname = doctorInfoData["surname"] as! String
                doctor.name = doctorInfoData["name"] as! String
                doctor.patronymic = doctorInfoData["patronymic"] as! String
                doctor.speciality = doctorInfoData["speciality"] as! String
                doctor.cabinet_number = doctorInfoData["cabinet"] as! String
                doctor.cabinet_phone = doctorInfoData["phone"] as! String
                
                doctors.append(doctor)
            }
            
        } catch {
            print(error)
        }
    }
    
    func loadUserData() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let user_login = json["login"] as? String {
                login = user_login
            }
            
            if let user_password = json["password"] as? String {
                password = user_password
            }
        } catch {
            print(error)
        }
    }
}
