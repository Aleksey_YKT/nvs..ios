//
//  RecordDetailsViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordDetailsViewController: UIViewController {

    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cabinetLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cabinetTitleLabel: UILabel!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    
    var login = ""
    var password = ""
    
    var record = Record()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        specialityLabel.text = record.specilality
        doctorLabel.text = record.doctor
        timeLabel.text = CustomDateFormatter(recordTime: record.date)
        cabinetLabel.text = record.cabinet
        phoneLabel.text = record.phone
        
        if record.cabinet == "" {
            cabinetTitleLabel.isHidden = true
        }
        
        if record.phone == "" {
            phoneTitleLabel.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelRecordButtonPressed(_ sender: Any) {
        let requestParams = "login=" + login + "&password=" + password + "&record=" + String(record.id)
        
        var request = URLRequest(url: URL(string: siteURL + postCancelRecordURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере")
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
                
                let alertMessage = UIAlertController(title: "Ошибка", message: "Повторите еще раз", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                _ = String(data: data, encoding: .utf8)
                
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "backToMenuView2", sender: self)
                }
                
                let alertMessage = UIAlertController(title: "Готово", message: "Вы успешно отменили запись", preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
        
        task.resume()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
