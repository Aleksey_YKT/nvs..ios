//
//  RecordsListTableViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class RecordsListTableViewController: UITableViewController {

    var login = ""
    var password = ""
    var records = [Record]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let userData = loadUserData()
        parseJSON(userData.data(using: .utf8)!)
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return records.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recordTableCell", for: indexPath) as! RecordsListTableViewCell
        
        cell.timeLabel?.text = CustomDateFormatter(recordTime: records[(indexPath as NSIndexPath).row].date)
        cell.doctorLabel?.text = records[(indexPath as NSIndexPath).row].doctor
        cell.specialityLabel?.text = records[(indexPath as NSIndexPath).row].specilality
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "openRecordDetailsView" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! RecordDetailsViewController
                destinationController.record = records[(indexPath as NSIndexPath).row]
                destinationController.login = login
                destinationController.password = password
            }
        }
    }
    
    func getDataFromServer() {
        let getRecords = "?login=" + login + "&password=" + password
        
        let request = URLRequest(url: URL(string: siteURL + getRecordsListURL + getRecords)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseRecordsData(data)
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        task.resume()
    }
    
    func parseRecordsData(_ data: Data) {
        do {
            guard let recordsData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert specialities to JSON")
                return
            }
            
            for recordData in recordsData {
                let rec = Record()
                
                rec.id = recordData["id"] as! Int
                rec.date = recordData["date"] as! String
                rec.doctor = recordData["doctor"] as! String
                rec.specilality = recordData["speciality"] as! String
                rec.cabinet = recordData["cabinet"] as! String
                rec.phone = recordData["phone"] as! String
                
                records.append(rec)
            }
            
        } catch {
            print(error)
        }
    }

    
    func loadUserData() -> String {
        var userFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            
            //reading
            do {
                userFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userFile
    }
    
    func parseJSON(_ data: Data) {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                print("error trying to convert posts to JSON")
                return
            }
            
            if let user_login = json["login"] as? String {
                login = user_login
            }
            
            if let user_password = json["password"] as? String {
                password = user_password
            }
        } catch {
            print(error)
        }
    }

}
