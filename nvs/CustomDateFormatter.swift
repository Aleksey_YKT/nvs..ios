//
//  CustomDateFormatter.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

func CustomDateFormatter(recordTime: String) -> String {
    var formattedRecordTime = String()
    
    if (recordTime.characters.count > 0) {
        let dateString = recordTime
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ru_RU_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "d MMMM HH:mm"
        formattedRecordTime = dateFormatter.string(from: dateObj!)
    }
    
    return formattedRecordTime
}
