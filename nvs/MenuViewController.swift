//
//  MenuViewController.swift
//  nvs
//
//  Created by Aleksey Ivanov on 02.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    var login = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated:true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        revomoeUserData()
    
        self.performSegue(withIdentifier: "openAuthView", sender: self)
    }

    func revomoeUserData() {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent("user.txt")
            let responseString = ""
            
            // Сохранение в файл
            do {
                try responseString.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                print("Файл сохранен")
            } catch {
                print("Ошибка при сохранении в файл")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
