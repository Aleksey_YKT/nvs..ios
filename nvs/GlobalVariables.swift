//
//  GlobalVariables.swift
//  nvs
//
//  Created by Aleksey Ivanov on 03.06.17.
//  Copyright © 2017 Hutec llc. All rights reserved.
//

import Foundation

public let userData = "user_data.txt"

public let siteURL = "http://185.22.60.76:8000/"

public let loginUserURL = "api/mobile/login_user/"

public let getRecordSpecialitiesURL = "api/mobile/record_patient/get_specialities/"
public let getRecordDoctrosURL = "api/mobile/record_patient/get_doctors/"
public let getRecordTimesURL = "api/mobile/record_patient/get_times/"
public let postRecordPatientURL = "api/mobile/record_patient/record/"

public let getRecordsListURL = "api/mobile/get_records/"
public let postCancelRecordURL = "api/mobile/cancel_record/"

public let getPolyclinicInfoURL = "api/mobile/get_polyclinic_info/"
public let getDoctorsInfoURL = "api/mobile/get_doctors_info/"
public let getPatientInfoURL = "api/mobile/get_patient_info/"
